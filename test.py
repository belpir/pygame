#!/usr/bin/python3

from random import randint as rnd


#fce
def hod(x=6):
    """
        hod() 1-6
        hod(7) 1-7
        hod(n) 1-n
        
        vytvořil: Gee
        editoval: Bel
        #############
    """

    # test vstupních hodnot
    assert isinstance(x, (int, float)), "Parametr x musí být číslo. Zadal jsi neplatný typ: {}".format(type(x))

    # samotné provádění kódu
    return rnd(1,x)


for i in range(10):
    print(hod(4))

